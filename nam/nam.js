#!/usr/local/bin/node

var http = require('http');

function pianoScale() {

  function round(freq) {
    return Math.round(freq*100)/100;
  }

  var freqs = {};
  freqs['A4'] = 440;
  var notes = "CDEFGAB".split('');
  var naturals = 'n|n|nn|n|n|n'.split('');
  var semitone = Math.pow(2,1/12.0);
  var forwardOctave = 4; var backwardOctave = 4;
  var forwardFreq = 440; var backwardFreq = 440;
  var forwardNote = 5; var backwardNote = 5;
  var forwardNat = 9; var backwardNat = 9;
  for (var i = 0; i < 48; i++) { //piano begins at A0
    forwardFreq *= semitone;
    backwardFreq /= semitone;
    forwardNat = (forwardNat + 1) % 12;
    backwardNat = (backwardNat + 11) % 12;

    if (naturals[backwardNat] == 'n') {
      if (backwardNote == 0) backwardOctave--;
      backwardNote = (backwardNote + 6) % 7;
      freqs[notes[backwardNote] + backwardOctave] = round(backwardFreq);
    } else {
      freqs[notes[backwardNote] + 'b' + backwardOctave] = round(backwardFreq);
      freqs[notes[(backwardNote + 6) % 7] + '#' + backwardOctave] = round(backwardFreq);
    }

    if (i > 38) continue; //piano ends at C8

    if (naturals[forwardNat] == 'n') {
      if (forwardNote == 7 - 1) forwardOctave++;
      forwardNote = (forwardNote + 1) % 7;
      freqs[notes[forwardNote] + forwardOctave] = round(forwardFreq);
    } else {
      freqs[notes[forwardNote] + '#' + forwardOctave] = round(forwardFreq);
      freqs[notes[(forwardNote + 1) % 7] + 'b' + forwardOctave] = round(forwardFreq);
    }
  }

  return freqs;
}

function tokenize(strm) {
  strm = strm.split('');

  function Token(cl,ln,st) {
    this.col = cl;
    this.line = ln;
    this.string = st;
  }

  var tokens = new Array();

  var col = 0;
  var line = 0;
  function advance(chr) {
    col++;
    if (chr == '\n') {
      line++;
      col = 0;
    }
  }

  function SPACE(){};
  function TOKEN(){};
  function COMMENT(){};

  var state = SPACE;

  var buf = '';
  var tokenCol = 0;
  var tokenLine = 0;
  var whitespace = " \t\r\n";

  tokenLoop: while(true) {
    var chr = strm.shift();
    switch(state) {
      case SPACE:
        if (chr == undefined) break tokenLoop;

        if (chr == ';') {
          state = COMMENT;
          break;
        }

        if (whitespace.indexOf(chr) == -1) {
          strm.unshift(chr);
          state = TOKEN;
          tokenCol = col+1;
          tokenLine = line+1;
          break;
        }

        advance(chr);
        break;

      case TOKEN:
        if (chr == ';') throw new Error('token interrupted - col: ' + (col+1) + ' line: ' + (line+1));
        if (chr == undefined) {
          tokens.push(new Token(tokenCol,tokenLine,buf));
          buf = '';
          break tokenLoop;
        }

        if (whitespace.indexOf(chr) != -1) {
          tokens.push(new Token(tokenCol,tokenLine,buf));
          buf = '';
          strm.unshift(chr);
          state = SPACE;
          break;
        }

        buf += chr;
        advance(chr);

        break;

      case COMMENT:
        if (chr == undefined) break tokenLoop;

        advance(chr);

        if (chr == '\n') state = SPACE;

        break;

      default:
        throw new Error('invalid state - col: ' + col + ' line: ' + line);
        break;
    }
  }

  return tokens;
}

function preprocessTokens(tokens) {
  var processed = new Array();

  var registers = {'i':0,'j':0,'k':0};

  var operators = "!:*@\"%~]_ijk";

  var processedTokens = new Array();

  for (var i = 0; i < tokens.length; i++) {
    var token = tokens[i];
    var str = token.string;
    var operator = str[str.length-1];
    if (operators.indexOf(operator) == -1) 
      throw new Error('unrecognized operator "' + operator + '" - col: ' + token.col + ' line: ' + token.line);
    token.operator = operator;
    token.expression = token.string.slice(0,-1);

    if ("ijk".indexOf(token.operator) != -1) {
      var val = undefined;
      try {
        val = eval(token.expression);
      } catch(e) {
        throw new Error('problem evaluating token - col: ' + token.col + ' line: ' + token.line);
      }
      if (val == undefined || val.constructor.name != 'Number')
        throw new Error('token expression "' + token.expression + '" does not express a number - col: ' + token.col + ' line: ' + token.line);
      registers[token.operator] = val;
    } else {
      processedTokens.push(token);
    }

  }

  var scale = pianoScale();

  for (var i = 0; i < processedTokens.length; i++) {
    var token = processedTokens[i];

    if (token.expression != '' && '!*_'.indexOf(token.operator) != -1) {
      throw new Error("'" + token.operator + "'" + ' operator does not take an operand - col: ' + token.col + ' line: ' + token.line);
    }
    
    if (token.expression == '' && ':"~'.indexOf(token.operator) != -1) {
      throw new Error("'" + token.operator + "'" + ' operator requires an operand - col: ' + token.col + ' line: ' + token.line);
    }

    "ijk".split('').forEach(function(chr) {
      token.expression = token.expression.replace(chr,registers[chr]);
    });

    for (var key in scale) {
      token.expression = token.expression.replace(key,scale[key]);
    }

    try {
      token.value = eval(token.expression);
    } catch(e) {
      throw new Error('problem evaluating token operand - col: ' + token.col + ' line: ' + token.line);
    }
  }

  return processedTokens;
}

function defineVoices(tokens) {
  function Voice(i,ch,typ,prm1,prm2) {
    this.id = i;
    this.channel = ch;
    this.type = typ;
    this.paramA = prm1;
    if (prm2 != undefined) 
      this.paramB = prm2;
  }
  var square = 'square';
  var noise = 'noise';
  var lastID = 0;
  var channel = 0;

  function centsDiff(i,j) {
    var x = i;
    var y = j;
    if (x < y) {
      x = j; 
      y = i;
    }
    return Math.log(x/y)/Math.log(Math.pow(2,1/(12*100)));
  }

  var voices = [new Array(),new Array()];

  var registers = [{'~':0,'%':0,'"':0},{'~':0,'%':0,'"':0}];

  for (var i = 0; i < tokens.length; i++) {
    var token = tokens[i];

    switch(token.operator) {
      case '~':
        registers[channel]['~'] = token.value;
        break;

      case '%':
        registers[channel]['%'] = token.value;
        break;

      case ':':
        channel = token.value;
        break;

      case ']':
        var dc = registers[channel]['%'];
        dc = Math.round((dc/100)*255);
        if (dc < 0) dc = 0;
        if (dc > 255) dc = 255;

        var id = null;
        for (var j = 0; j < voices[channel].length; j++) {
          var voice = voices[channel][j];
          if (voice.type == square
           && voice.paramA == dc) {
            id = voice.id;
            token.voice = id;
            break;
          }
        }
        if (id != null) break;

        token.voice = lastID++;
        voices[channel].push(new Voice(token.voice,channel,square,dc));

        break;

      case '*':
        var steps = registers[channel]['%'];
        steps = Math.round(steps/100);

        var f = registers[channel]['~'];
        if (f < 1) f = 1;
        if (f > 44100) f = 44100;

        var step = Math.round(44100/f);

        var id = null;
        for (var j = 0; j < voices[channel].length; j++) {
          var voice = voices[channel][j];
          if (voice.type == noise 
           && voice.paramA == step 
           && voice.paramB == steps) {
            id = voice.id;
            token.voice = id;
            break;
          }
        }
        if (id != null) break;

        token.voice = lastID++;
        voices[channel].push(new Voice(token.voice,channel,noise,step,steps));

        break;

    }
  }

  return voices;
}

function instructions(tokens,voiceArray) {
  var voices = voiceArray[0].concat(voiceArray[1]);

  var lines = [];

  lines.push('var ac = new window.AudioContext();');
  lines.push('var voices = new Array();');
  lines.push('');
  lines.push('function play(t) {');
  lines.push('');
  lines.push('  var master = ac.createGain();');
  lines.push('  master.gain.value = 1;');
  lines.push('');
  lines.push('  var a = ac.createGain();');
  lines.push('  a.gain.value = 1;');
  lines.push('');
  lines.push('  var b = ac.createGain();');
  lines.push('  b.gain.value = 1;');
  lines.push('');

  lines.push('  //generate voices');
  for (var i = 0; i < voices.length; i++) {
    var voice = voices[i];

    if (voice.type == 'square') {
      lines.push('  voices.push((function() {');
      lines.push('    //voice ' + voice.id + ';');
      lines.push('    var o = ac.createOscillator();');
      lines.push('    o.type = "sawtooth";');
      lines.push('    o.frequency.value = 0;');
      lines.push('    o.start();');
      lines.push('');
      lines.push('    var pwm = ac.createWaveShaper();');
      lines.push('');
      lines.push('    var pwa = new Float32Array(256);');
      lines.push('    for (var i = 0; i < 256; i++) {');
      lines.push('      if (i < ' + voice.paramA + ') {');
      lines.push('        pwa[i] = 1;');
      lines.push('      } else {');
      lines.push('        pwa[i] = -1;');
      lines.push('      }');
      lines.push('    }');
      lines.push('');
      lines.push('    pwm.curve = pwa;');
      lines.push('    o.connect(pwm);');
      if (voice.channel == 0) {
        lines.push('    pwm.connect(a);');
      } else {
        lines.push('    pwm.connect(b);');
      } 
      lines.push('');
      lines.push('    return o;');
      lines.push('  })());');
    }

    if (voice.type == 'noise') {
      lines.push('  voices.push((function() {');
      lines.push('    //voice ' + voice.id + ';');
      lines.push('    var buf = ac.createBuffer(1,' + (voice.paramA*voice.paramB) + ',44100);');
      lines.push('    var data = buf.getChannelData(0);');
      lines.push('    var bit = 0;');
      lines.push('    for (var i = 0; i < ' + (voice.paramA*voice.paramB) + '; i++) {');
      lines.push('      if (i % ' + voice.paramA + ' == 0) ');
      lines.push('        bit = Math.round(Math.random())*2-1;');
      lines.push('      data[i] = bit;');
      lines.push('    }');
      lines.push('');
      lines.push('    var src = ac.createBufferSource();');
      lines.push('    src.buffer = buf;');
      lines.push('    src.loop = true;');
      lines.push('    src.playbackRate.value = 0;');
      lines.push('    src.start();');
      lines.push('');
      if (voice.channel == 0) {
        lines.push('    src.connect(a);');
      } else {
        lines.push('    src.connect(b);');
      } 
      lines.push('');
      lines.push('    return src;');
      lines.push('');
      lines.push('  })());');
    }

    lines.push('');
  }

  lines.push('');
  lines.push('  //wiring');
  lines.push('  a.connect(master);');
  lines.push('  b.connect(master);');
  lines.push('  master.connect(ac.destination);');

  lines.push('');
  lines.push('  //generate transitions');

  var registers = [{'~':0,'%':0,'"':0},{'~':0,'%':0,'"':0}];

  var timerA = [0,0];
  var timerB = [0,0];

  var channel = 0;

  for (var i = 0; i < tokens.length; i++) {
    var token = tokens[i];
    switch(token.operator) {
      case '~':
        registers[channel]['~'] = token.value;
        break;

      case '%':
        registers[channel]['%'] = token.value;
        break;

      case '"':
        registers[channel]['"'] = token.value;
        break;

      case ':':
        channel = token.value;
        break;

      case '!':
        if (timerA[0] > timerA[1]) {
          timerA[1] = timerA[0];
        } else {
          timerA[0] = timerA[1];
        }
        timerB[0] = timerA[0];
        timerB[1] = timerA[0];
        break;

      case '@':
        if (token.value == undefined) {
          lines.push('  ' + ((channel == 0) ? 'a' : 'b') + '.gain.setValueAtTime(' + (registers[channel]['%']/100) + ',t+' + (timerB[channel]) + ');');
        } else {
          lines.push('  ' + ((channel == 0) ? 'a' : 'b') + '.gain.setValueAtTime(' + (registers[channel]['%']/100) + ',t+' + (timerB[channel]) + ');');
          lines.push('  ' + ((channel == 0) ? 'a' : 'b') + '.gain.linearRampToValueAtTime(' + (token.value/100) + ',t+' + (timerB[channel]+registers[channel]['"']) + ');');
          registers[channel]['%'] = token.value;
          timerB[channel] += registers[channel]['"'];
        }
        break;

      case ']':
        var voice = voices[token.voice];

        if (token.value == undefined) {
          lines.push('  voices[' + (token.voice) + '].frequency.setValueAtTime(' + (registers[channel]['~']) + ',t+' + (timerA[channel]) + ');');
          lines.push('  voices[' + (token.voice) + '].frequency.setValueAtTime(0,t+' + (timerA[channel]+registers[channel]['"']) + ');');
        } else {
          lines.push('  voices[' + (token.voice) + '].frequency.setValueAtTime(' + (registers[channel]['~']) + ',t+' + (timerA[channel]) + ');');
          lines.push('  voices[' + (token.voice) + '].frequency.linearRampToValueAtTime(' + (token.value) + ',t+' + (timerA[channel]+registers[channel]['"']) + ');');
          lines.push('  voices[' + (token.voice) + '].frequency.setValueAtTime(0,t+' + (timerA[channel]+registers[channel]['"']) + ');');
          registers[channel]['~'] = token.value;
        }

        timerA[channel] += registers[channel]['"'];
        timerB[channel] = timerA[channel];
        break;

      case '*':
        var voice = voices[token.voice];

        lines.push('  voices[' + (token.voice) + '].playbackRate.setValueAtTime(1,t+' + (timerA[channel]) + ');');
        lines.push('  voices[' + (token.voice) + '].playbackRate.setValueAtTime(0,t+' + (timerA[channel]+registers[channel]['"']) + ');');

        timerA[channel] += registers[channel]['"'];
        timerB[channel] = timerA[channel];
        break;

      case '_':
        timerA[channel] += registers[channel]['"'];
        timerB[channel] = timerA[channel];
        break;

    }
  }

  lines.push('');
  lines.push('};');
  lines.push('play(ac.currentTime);');

  return lines.join('\n');
};

//4k of padding...
var padding = (new Array(4096)).join(' ') + "\n";

var pokeQueue = new Array();
var refreshQueue = new Array();
var sendQueue = new Array();
var recvQueue = new Array();

function poke() {
  var pk = pokeQueue.shift();
  if (pk != undefined) return pk();
  refreshQueue.push(Promise.resolve());
}

function refresh() {
  var rsh = refreshQueue.shift();
  if (rsh != undefined) return rsh;

  return new Promise(function(res,rej) {
    pokeQueue.push(res);
  });
}

function send(val) {
  var snd = sendQueue.shift();
  if (snd != undefined) return snd(val);
  recvQueue.push(Promise.resolve(val));
}
//initial document
send("<script>document.title = 'ready...';\n" +
     "document.write('ready...<br/>');</script>\n");

function recv() {
  var rcv = recvQueue.shift();
  if (rcv != undefined) return rcv;

  return new Promise(function(res,rej) {
    sendQueue.push(res);
  });
}

http.createServer(function(req,rsp) {
  var header = {
    'Content-Type': 'text/html; charset=UTF-8',
  };

  if (req.method == 'PUT') {
    rsp.writeHead(200,header);
    var body = '';
    req.on('data',function(chunk) {
      body += chunk;
    });
    req.on('end', function() {
      poke();

      var js = '';
      var status = "Playing...\n";
      try {
        var tokens = tokenize(body);
        tokens = preprocessTokens(tokens);
        var voices = defineVoices(tokens);
        var js = instructions(tokens,voices);
      } catch(e) {
        status = "Problem:\n\t" + e.message + "\n";
      }

      send("<script>" + js + "</script>\n");
      rsp.end(status);

      console.log(status);
    });
    return;
  }

  if (req.url == '/out') {
    rsp.writeHead(200,header);
    recv().then(function(val) {
      rsp.write(padding + val + "\n",function() {
        refresh().then(function() {
          rsp.end('<script>document.location.reload();</script>');
        });
      });
    });
    return;
  }

  rsp.writeHead(404,header);
  rsp.end();

}).listen(8000);
