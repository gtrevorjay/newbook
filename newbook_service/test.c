#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(void) {
  int snd_fd = open("/run/newbook/sounds",O_WRONLY);
  unsigned int buf = 0;
  buf = 0;
  buf |= 2 << 30;
  buf |= (0xFFFFF & 19600) << 10;
  buf |= (0x3FF & 667);
  write(snd_fd,&buf,4);

  buf = 0;
  buf |= 2 << 30;
  buf |= (0xFFFFF & 0) << 10;
  buf |= (0x3FF & 10);
  write(snd_fd,&buf,4);

  buf = 0;
  buf |= 2 << 30;
  buf |= (0xFFFFF & 32963) << 10;
  buf |= (0x3FF & 667);
  write(snd_fd,&buf,4);

  buf = 0;
  buf |= 2 << 30;
  buf |= (0xFFFFF & 0) << 10;
  buf |= (0x3FF & 10);
  write(snd_fd,&buf,4);

  buf = 0;
  buf |= 2 << 30;
  buf |= (0xFFFFF & 26163) << 10;
  buf |= (0x3FF & 667);
  write(snd_fd,&buf,4);
}
