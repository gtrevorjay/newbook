/* gcc -Wall -o newbook newbook.c -lwiringPi -I/usr/include/wiringPi */
#include <wiringPi.h>
#include <signal.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>


/* handle CTRL+C */
static volatile unsigned char active = 1;
void quit(int ign) {
  active = 0;
  return;
}

int snd_fd = 0;
void sound_drain(void) {
  unsigned int snd;
  int ret;
  do {
    ret = read(snd_fd,&snd,4);
  } while(ret != -1 && ret != 0);
  return;
}

int main(void) {
  /* handle CTRL+C */
  signal(SIGINT, quit);

  /* need root */
  wiringPiSetup () ;

  /* setup pins */
  for (int i = 0; i < 4; i++) {
    pinMode(26+i, OUTPUT);
    digitalWrite(26+i, HIGH);
    pinMode(22+i, INPUT);
    pullUpDnControl(22+i,PUD_UP);
    if (i == 3) continue; /* only three buttons */
    pinMode(7+i, INPUT);
    pullUpDnControl(7+i,PUD_UP);
  }
  pinMode(1, OUTPUT);
  digitalWrite(1, LOW);

  /* files */
  int cmd_fd = open("/run/newbook/commands",O_NONBLOCK|O_RDONLY);
  snd_fd = open("/run/newbook/sounds",O_NONBLOCK|O_RDONLY);
  printf("Waiting for client...\n");
  int btn_fd = open("/run/newbook/buttons",O_WRONLY);
  printf("...connected!\n");
  if (cmd_fd == -1 || btn_fd == -1 || snd_fd == -1) {
    printf("Problem opening needed interface files.\n");
    active = 0;
  }

  /* sound */
  unsigned char sound_on = 0;
  double sound_duty_cycle = 0.0;
  double sound_freq = 0.0;
  double sound_togo = 0.0;
  /* sound */

  /* state */
  unsigned char switches_stable, switches_pending, switches_ready = 0;
  unsigned int switches_duration = 0;
  /* state */

  unsigned int elapsed, last = millis();
  while(active) {
    delayMicroseconds(4167); /* ~240Hz */
    elapsed = millis() - last; 
    if (elapsed > 1000) elapsed = 1000;
    last = millis();

    unsigned int switches_now = 0;
    for (int i = 0; i < 7; i++) {
      int j = 22+i;
      if (i > 3) j = (i % 3) + 7; 
      switches_now = switches_now | (!digitalRead(j) << i);
    }

    if (switches_now == switches_pending) {
      switches_duration += elapsed;
    } else {
      switches_pending = switches_now;
      switches_duration = 0;
    }

    if (switches_duration > 50 && 
        switches_now != switches_stable) {
      if (((switches_now & 0x70)^(switches_stable & 0x70)) && switches_ready) {
        unsigned char buf = ((switches_now & 0x70)^(switches_stable & 0x70))|
             (switches_now & 0x0F);
        if (write(btn_fd,&buf,1) == -1) {
          printf("Problem writing to interface file.\n"); 
          active = 0;
        }
      }
      switches_stable = switches_now;
      switches_ready = 1;
      sound_on = 0;
      sound_drain();
      continue;
    }

    unsigned char cmd;
    int ret = read(cmd_fd,&cmd,1);
    if (ret != -1 && ret != 0) {

      if ((cmd & 0xF0) == 0) {
        for (int i = 0; i < 4; i++) 
          digitalWrite(26 + i, (cmd >> i)&1);
        sound_on = 0;
        sound_drain();
        continue;
      }

      if (((cmd & 0xF0) >> 4) == 1 &&
          (cmd & 0x0F) != 0) {
        /* printf("Turning on sound.\n"); */
        sound_on = 1;
        continue;
      }

      if (((cmd & 0xF0) >> 4) == 1 &&
          (cmd & 0x0F) == 0) {
        /* printf("Turning off sound.\n"); */
        sound_on = 0;
        sound_drain();
        continue;
      }

    }


    if (!sound_on) {
      pinMode(1, OUTPUT);
      digitalWrite(1, LOW);
      continue;
    }

    for (int i = 0; i < 4; i++) 
      digitalWrite(26+i, LOW);
    
    if (sound_togo == 0) {
      unsigned int snd;
      ret = read(snd_fd,&snd,4);

      if (ret != 4) {
        sound_on = 0;
        sound_drain();
        continue;
      }

      const double cycles[] = {0.125, 0.25, 0.5};
      sound_duty_cycle = cycles[(0xC0000000 & snd) >> 30];
      sound_freq = ((double) ((0x3FFFFC00 & snd) >> 10)) / 100.0;
      sound_togo = (0x3FF & snd);

      /* printf("%g %g %g\n",sound_duty_cycle,sound_freq,sound_togo); */

      const double sound_t = 19.2e6;
      int sound_clock = sound_t/sound_freq/4095.0;
      if (sound_clock < 1) sound_clock = 1;
      int sound_range = sound_t/sound_clock/sound_freq;
      if (sound_clock > sound_range) {
        int tmp = sound_clock;
        sound_clock = sound_range;
        sound_range = tmp;
      }

      if (sound_freq > 10.0 && sound_freq < 8000.0) {
        pinMode(1, PWM_OUTPUT);
        pwmSetMode(PWM_MODE_MS);
        pwmSetClock(sound_clock);  
        pwmSetRange(sound_range) ;
        pwmWrite (1, sound_range * sound_duty_cycle);
      } else {
        pinMode(1, OUTPUT);
        digitalWrite(1, LOW);
      }
    }

    sound_togo -= elapsed;
    if (elapsed > sound_togo)
      sound_togo = 0;

  }

  printf("\nquiting...\n");

  /* cleanup GPIO */
  for (int i = 0; i < 4; i++) 
    digitalWrite(26+i, LOW);
  pinMode(1, OUTPUT);
  digitalWrite(1, LOW);

  /* cleanup files */
  close(cmd_fd);
  close(btn_fd);
  close(snd_fd);

  return 0;
}
