#!/usr/bin/python
import time

with open('/run/newbook/command', 'wb') as output:
    now = 1
    dr = True
    while True:
        time.sleep(.18)

        if dr:
            now = now * 2
        else:
            now = now / 2
            
        if now == 8:
            dr = False
        if now == 1:
            dr = True

        output.write(chr(now))
        output.flush()
