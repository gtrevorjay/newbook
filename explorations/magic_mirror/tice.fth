\ ######## monitor ########
variable ram 128 cells allot
: init_ram
  128 0 do
    0 ram i cells + !
  loop
;
init_ram

variable ptr
0 ptr !
variable offset_val

: offset++
  offset_val @
  dup 0= if 0 ptr ! then 
  dup 1 + offset_val !
;

variable wh_val
variable rh_val

: wh
  wh_val @
  dup invert wh_val !
;

: rh
  rh_val @
  dup invert rh_val !
;

: reset_others
  dup [ ' wh ] literal  <> if false wh_val ! then
  dup [ ' rh ] literal <> if true rh_val ! then
  dup [ ' offset++ ] literal <> if 0 offset_val ! then
drop ;
ram reset_others

: bits ( %b -- n )
  0 swap \ initial value
    4 0 do
      dup 2 mod if swap 1 i lshift + swap then
      dup if 10 / then
    loop
drop ;

: bars cr 4 0 do 35 emit loop cr ;

: leds ( n -- %b)
  bars
  4 0 do
    dup 
    1 3 i - lshift
    and
    \ s" gpio write 2X Y"
    \ swap
    \ rot
    if 49 else 48 then emit
    \ if
    \  dup 49 swap 14 + c!
    \ else
    \   dup 48 swap 14 + c!
    \ then
    \ dup 57 i - swap 12 + c!
    \ swap system
  loop
  bars
drop ;

variable tick_val
: tick 
  tick_val @ execute
;

: a 
  bits
  offset++ 4 * lshift
  ptr @
  or
  ptr !
  [ ' offset++ ] literal reset_others
  tick
;

: w
  ptr @ \ save current ptr
  swap
    bits
    ram ptr @ cells + @   
    wh if 
        15 and
        swap
        4 lshift
        ptr @ 1 - ptr !
      else
        15 4 lshift and
      then
    or
  swap
  cells ram + !
  [ ' wh ] literal reset_others
  tick
;

: r
  ram ptr @ cells + @
  rh if 
      4 rshift
    else
      ptr @ 1 + ptr !
    then
  leds
  [ ' rh ] literal reset_others
  tick
;
\ ######## monitor ########

: player
  ram 128 cells + 
;

create line_vals 6 , 7 , 8 , 3 , 4 , 5 , 0 , 1 , 2 ,
                 6 , 3 , 0 , 7 , 4 , 1 , 8 , 5 , 2 , 
                 6 , 4 , 2 , 0 , 4 , 8 ,
: init_lines
  24 0 do 
    line_vals i cells + @
    ram 103 i + cells + !
  loop
;
init_lines
: lines ( i j -- addr )
  swap
  3 * + 
  ram swap 103 + cells + 
;

: nxt
  ram 102 cells + 
;

: nxt_val
  ram 101 cells + 
;

create pref_vals 6 , 8 , 0 , 2 , 7 , 5 , 1 , 3 ,
: init_prefs
  8 0 do
    pref_vals i cells + @
    ram 92 i + cells + !
  loop
;
init_prefs
: prefs
  ram 92 rot + cells + 
;

: complete
  ram 91 cells + 
;

: count
  ram 90 cells + 
;

: clear_board
  15 0 do 
    0 ram i cells + !
  loop
  -1 nxt !
  false complete !
;

: menu
  ram 15 cells + @
  0= invert if 
      ram 15 cells + @
      15 = if
        bye
      then 
      ram 15 cells + @
      player !
      clear_board
    then
  0 ram 15 cells + !
;

: turn ( -- X or O )
  0 
  9 0 do
    ram i cells + @
    0= if 1 + then
  loop
  2 mod 
  0= if 
      2
     else
      1
     then
;

: val ( n -- val )
  dup nxt @ = if 
      drop
      nxt_val @
    else
      ram swap cells + @
    then
;

: is_win ( n code -- win? )
  8 0 do
    0 count !
    i
    3 0 do
      i
      ( n c i j )
      2dup lines @
      ( n c i j p )
      dup 5 pick =
      ( n c i j p isn )
      swap val 4 pick = or
      ( n c i j isnc )
      if count @ 1 + count ! then
      drop
    loop
    drop 
    count @ 3 = if leave then
  loop
  count @ 3 = if
      true
    else
      false
    then
  ( n c isw )
  -rot
drop drop ;

: closed
  val 0<>
;

: bad_from_good ( sym -- !sym )
  1 = if
      2
    else
      1
    then
;

: forked ( n good -- t/f )
  0 ( n good ops )
  8 0 do
    i swap ( n good i ops )
    0 ( n good i ops count )
    0 ( n good i ops count blk )
    false ( n good i ops count blk in )
    3 0 do 
      i
      swap >r swap >r swap >r swap >r
      r> r> r> r>
      ( n good i j ops count blk in )
      5 pick 5 pick lines @
      ( n good i j ops count blk in v )
      dup
      ( n good i j ops count blk in v v )
      9 pick = 
      ( n good i j ops count blk in v in' )
      rot or swap
      ( n good i j ops count blk in v )
      val
      ( n good i j ops count blk in *v )
      dup 
      ( n good i j ops count blk in *v *v )
      8 pick = if
          >r >r >r 1 + r> r> r>
        then
      ( n good i j ops count blk in *v )
      7 pick bad_from_good = if
          swap 1 + swap
        then 
      ( n good i j ops count blk in )
      >r >r >r >r drop r> r> r> r>
    loop
    swap 0= and if
        +
      else
        drop
      then
    ( n good i ops )
    swap drop
    ( n good ops )
  loop
  1 > if
      true
    else
      false
    then
  ( n good forked )
  -rot  
drop drop ;

: to_win
  9
  9 0 do
    i closed invert i turn is_win and if
        drop 
        i
        \ ." to_win"
        leave
      then
  loop
;

: to_block
  9
  9 0 do
    i closed invert i turn bad_from_good is_win and if
        drop 
        i
        \ ." to_block"
        leave
      then
  loop
;

: to_fork
  9
  9 0 do
    i closed invert i turn forked and if
        drop 
        i
        \ ." to_fork"
        leave
      then
  loop
;

: to_block_fork_with_win
  9 ( mv )
  9 0 do
    i swap ( n mv )
    i turn bad_from_good forked if
      9 0 do
        i swap ( n np mv )
        i closed invert if 
          1 pick nxt !
          turn nxt_val !
          9 0 do
            i swap ( n np npp mv )
            i closed invert
            i turn is_win
            i turn bad_from_good forked invert
            and and
            if
              \ ." to_block_fork_with_win"
              drop 2dup drop \ mv = np
              swap drop
              leave
            then
            ( n np npp mv )
            swap drop
          loop
          -1 nxt !
          dup 9 <> if
            swap drop
            leave
          then
        then
        ( n np mv )
        swap drop
      loop
      dup 9 <> if
        swap drop
        leave
      then
    then
    ( n mv )
    swap drop ( mv )
  loop
;

: to_block_fork
  9
  9 0 do
    i closed invert i turn bad_from_good forked and if
        drop 
        i
        \ ." to_block_fork"
        leave
      then
  loop
;

: center
  4 closed invert if
      4
      \ ." center"
    else
      9
    then
;

: opposite
  9
  4 0 do
    i prefs @
    val turn bad_from_good = 
    3 i - prefs @ closed invert
    and if 
        drop
        3 i - prefs @
        \ ." opposite"
        leave
      then
  loop
;

: priorities
  8 0 do 
    i prefs @
    dup closed invert if 
        \ ." priorities"
        leave
      then
  loop
;

: move 
  1 0 do 
    to_win dup 9 < if leave else drop then
    to_block dup 9 < if leave else drop then
    to_fork dup 9 < if leave else drop then
    to_block_fork_with_win dup 9 < if leave else drop then
    to_block_fork dup 9 < if leave else drop then
    center dup 9 < if leave else drop then
    opposite dup 9 < if leave else drop then
    priorities dup 9 < if leave else drop then
  loop
;

: free
  false
  9 0 do 
    ram i cells + @
    0= if
      drop true
      leave
    then
  loop
;

: winner 
  0
  9 0 do
    i
    ram i cells + @
    is_win if 
      drop ram i cells + @
      leave
    then
  loop
;

: tock 
  menu
  turn player @ = free and if
    move ( mv )
    dup ram 10 cells + !
    turn swap ram swap cells + !
  then
  winner ram 11 cells + !
;
' tock tick_val !

: test 
  clear_board
  0110 a
  0001 w
  0100 a
  0010 w
  0010 a
  0001 w
;

: test2
  clear_board
  0110 a
  0010 w
  0100 a
  0001 w
  0010 a
  0001 w
;
