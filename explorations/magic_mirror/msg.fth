\ ######## monitor ########
variable ram 128 cells allot
: init_ram
  128 0 do
    0 ram i cells + !
  loop
;
init_ram

variable ptr
0 ptr !
variable offset_val

: offset++
  offset_val @
  dup 0= if 0 ptr ! then 
  dup 1 + offset_val !
;

variable wh_val
variable rh_val

: wh
  wh_val @
  dup invert wh_val !
;

: rh
  rh_val @
  dup invert rh_val !
;

: reset_others
  dup [ ' wh ] literal  <> if false wh_val ! then
  dup [ ' rh ] literal <> if true rh_val ! then
  dup [ ' offset++ ] literal <> if 0 offset_val ! then
drop ;
ram reset_others

: bits ( %b -- n )
  0 swap \ initial value
    4 0 do
      dup 2 mod if swap 1 i lshift + swap then
      dup if 10 / then
    loop
drop ;

: bars cr 4 0 do 35 emit loop cr ;

: leds ( n -- %b)
  bars
  4 0 do
    dup 
    1 3 i - lshift
    and
    \ s" gpio write 2X Y"
    \ swap
    \ rot
    if 49 else 48 then emit
    \ if
    \  dup 49 swap 14 + c!
    \ else
    \   dup 48 swap 14 + c!
    \ then
    \ dup 57 i - swap 12 + c!
    \ swap system
  loop
  bars
drop ;

variable tick_val
: tick 
  tick_val @ execute
;

: a 
  bits
  offset++ 4 * lshift
  ptr @
  or
  ptr !
  [ ' offset++ ] literal reset_others
  tick
;

: w
  ptr @ \ save current ptr
  swap
    bits
    ram ptr @ cells + @   
    wh if 
        15 and
        swap
        4 lshift
        ptr @ 1 - ptr !
      else
        15 4 lshift and
      then
    or
  swap
  cells ram + !
  [ ' wh ] literal reset_others
  tick
;

: r
  ram ptr @ cells + @
  rh if 
      4 rshift
    else
      ptr @ 1 + ptr !
    then
  leds
  [ ' rh ] literal reset_others
  tick
;
\ ######## monitor ########

: clear
  do 
    0 ram i cells + !
  loop
;

: tock 
  ram 127 cells + @
  dup 1 = if
    128 0 clear
  then
;
' tock tick_val !
