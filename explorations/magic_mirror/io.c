/* gcc -Wall -o io io.c -lwiringPi */
#include <stdio.h>
#include <wiringPi.h>
#include <signal.h>

static volatile int active = 1;
void quit(int ign) {
  active = 0;
}

int main (void) {
  wiringPiSetup () ;
  for (int i = 0; i < 7; i++) {
    pinMode(i, INPUT);
    pullUpDnControl(i,PUD_UP);
  }

  for (int i = 0; i < 4; i++) {
    pinMode(26+i, OUTPUT);
    digitalWrite(26+i, HIGH);
  }

  signal(SIGINT, quit);

  char current, last, now = 0;

  while(active) {
    now = 0;
    for (int i = 0; i < 7; i++) {
      now = now | (!digitalRead (i) << i);
    }

    if (last == now && now != current) {

      /*
      for (int i = 0; i < 4; i++)
        digitalWrite(26 + i, ((now >> i) & 1) == 1);
      */

      /* 01234 567 */
      /* 1 2 4 8 16 32 64 128 */

      if ((now & 48) != (current & 48)) {
        for (int i = 0 ; i < 4; i++) {
          printf("%d",(now >> (3 - i)) & 1);
        }
        printf(" ");
      }

      if ((now & 16) != (current & 16))
        printf("a \n");

      if ((now & 32) != (current & 32))
        printf("w \n");

      if ((now & 64) != (current & 64))
        printf("r \n");

      current = now;
    }

    last = now;
    delay (16) ;
  }

  for (int i = 0; i < 4; i++) 
    digitalWrite(26 + i, 0);

  return 0 ;
}
