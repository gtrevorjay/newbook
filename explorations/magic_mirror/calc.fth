\ ######## monitor ########
variable ram 128 cells allot
: init_ram
  128 0 do
    0 ram i cells + !
  loop
;
init_ram

variable ptr
0 ptr !
variable offset_val

: offset++
  offset_val @
  dup 0= if 0 ptr ! then 
  dup 1 + offset_val !
;

variable wh_val
variable rh_val

: wh
  wh_val @
  dup invert wh_val !
;

: rh
  rh_val @
  dup invert rh_val !
;

: reset_others
  dup [ ' wh ] literal  <> if false wh_val ! then
  dup [ ' rh ] literal <> if true rh_val ! then
  dup [ ' offset++ ] literal <> if 0 offset_val ! then
drop ;
ram reset_others

: bits ( %b -- n )
  0 swap \ initial value
    4 0 do
      dup 2 mod if swap 1 i lshift + swap then
      dup if 10 / then
    loop
drop ;

: bars cr 4 0 do 35 emit loop cr ;

: leds ( n -- %b)
  \ bars
  4 0 do
    dup 
    1 3 i - lshift
    and
    s" gpio write 2X Y"
    swap
    rot
    \ if 49 else 48 then emit
    if
      dup 49 swap 14 + c!
    else
      dup 48 swap 14 + c!
    then
    dup 57 i - swap 12 + c!
    swap system
  loop
  \ bars
drop ;

variable tick_val
: tick 
  tick_val @ execute
;

: a 
  bits
  offset++ 4 * lshift
  ptr @
  or
  ptr !
  [ ' offset++ ] literal reset_others
  tick
;

: w
  ptr @ \ save current ptr
  swap
    bits
    ram ptr @ cells + @   
    wh if 
        15 and
        swap
        4 lshift
        ptr @ 1 - ptr !
      else
        15 4 lshift and
      then
    or
  swap
  cells ram + !
  [ ' wh ] literal reset_others
  tick
;

: r
  ram ptr @ cells + @
  rh if 
      4 rshift
    else
      ptr @ 1 + ptr !
    then
  leds
  [ ' rh ] literal reset_others
  tick
;
\ ######## monitor ########


variable buffer 80 allot \ to hide underlying forth is ASCII

: >ascii
  dup 239 > swap dup 250 < rot and
  if 
    240 - 48 + \ 0-9
  else
     case
      \ 75 of 46 endof \ . 
      78 of 43 endof \ +
      92 of 42 endof \ *
      96 of 45 endof \ -
      97 of 47 endof \ /
        32 swap \ SP
    endcase 
  then
;

: >ebcdic
  dup 45 = if
    96
  else
    dup 47 > over 58 < and if
      48 - 240 +
    else
      drop
      0
    then
  then
;

: convert
  80 0 do
    ram i cells + @
    >ascii
    buffer i + c!
  loop
;

: clear
  do 
    0 ram i cells + !
  loop
;

: >str
  0 swap
  dup 0< if
    negate
    swap drop 1 swap
    45 ram 96 cells + !
  then
  s>d <# #s #>
  0 
  do
    dup i + c@
    2 pick
    96 +
    i +
    cells 
    ram +
    !
  loop
  drop drop
;

: calc
  depth if 
    depth 0 do drop loop
  then
  128 96 clear
  buffer 80 ['] evaluate catch
  \ swap drop swap 
  depth 2 < or
  if
    depth 0>
    if drop then
    197 ram 96 cells + !
    217 ram 97 cells + !
    217 ram 98 cells + !
  else
    >str
    32 0 do
      ram 96 i + cells + @
      >ebcdic
      ram 96 i + cells + !
    loop
  then
;

: tock 
  ram 80 cells + @
  dup 0= if
    128 0 clear
  then
  1 = if 
    convert
    calc
  then
  15 ram 80 cells + !
;
' tock tick_val !

: test
  242 ram 0 cells + !
  0 ram 1 cells + !
  242 ram 2 cells + !
  0 ram 3 cells + !
  78 ram 4 cells + !
;

: show
  128 0 do
    ram i cells + @ .
  loop
;

: show2
  80 0 do
    buffer i + c@ .
  loop
;
