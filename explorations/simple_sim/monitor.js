var tick = function(mem) {}

/* MONITOR */
function Memory(mem) {
  this.memory = mem;
}
Memory.prototype.peek = function(idx) {
  if (this.memory[idx] == undefined) 
    return 0;
  return this.memory[idx];
}
Memory.prototype.poke = function(idx, val) {
  this.memory[idx] = val & 0xFF;
}

function Newbook() {
  this.memory = {};
  this.offset = 0;
  this.ptr = 0;
  this.last_pressed = null;
  this.inner_tick = (function(mem) {
    var memory = new Memory(mem);
    return function() {
      tick(memory);
    }
  })(this.memory);
}

Newbook.prototype.calc_offset = function(ths) {
  if (ths == this.last_pressed) {
    this.offset += 4;
  } else {
    this.offset = 0;
  }
  this.last_pressed = ths;
  return this.offset;
}

Newbook.prototype.leds = function(val) {
  val = val.toString(2);
  while(val.length < 4)
    val = '0' + val;
  console.log('\n+-----------+');
  console.log('|  ' + val.split('').join(' ') + '  |');
  console.log('+-----------+\n');
}

Number.prototype.newbook = new Newbook();
Number.prototype.i = function() {
  var offset = this.newbook.calc_offset(this.i);
  if (offset >= 8) {
    offset = this.newbook.offset = 0;
    this.newbook.ptr--;
  }
  if (offset == 0) 
    this.newbook.memory[this.newbook.ptr] = 0;
  this.newbook.memory[this.newbook.ptr] |= this << offset;
  this.newbook.inner_tick();
}
Number.prototype.l = function() {
  var offset = this.newbook.calc_offset(this.l);
  if (offset == 0) this.newbook.ptr = 0;
  this.newbook.ptr |= this << offset;
}
Number.prototype.o = function() {
  var offset = this.newbook.calc_offset(this.o);
  if (offset >= 8) {
    offset = this.newbook.offset = 0;
    this.newbook.ptr++;
  }
  if (this.newbook.memory[this.newbook.ptr] == undefined) 
    this.newbook.memory[this.newbook.ptr] = 0;
  var val = this.newbook.memory[this.newbook.ptr];
  val = (val >> (4 - offset)) & 0xF;
  this.newbook.leds(val);
}
/* MONITOR */

tick = function(mem) {
  //example "adder" but your code here
  mem.poke(2,mem.peek(0)+mem.peek(1));
  //example "adder" but your code here
}
