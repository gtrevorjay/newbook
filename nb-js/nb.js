var nam = require('../nam-js/nam.js');
var fs = require('fs');

function Memory(mem) {
  this.memory = mem;
}
Memory.prototype.peek = function(idx) {
  if (this.memory[idx] == undefined) 
    return 0;
  return this.memory[idx];
}
Memory.prototype.poke = function(idx, val) {
  this.memory[idx] = val & 0xFF;
}

function Newbook() {
  this.memory = new Memory({});
  this.offset = 0;
  this.ptr = 0;
  this.last_pressed = null;
  this.tick = null;
  this.inner_tick = function() {
    this.tick(this.memory);
  }

  this.snd_fd = fs.openSync('/run/newbook/sounds','w');
  this.cmd_fd = fs.openSync('/run/newbook/commands','w');
  this.btn_fd = fs.openSync('/run/newbook/buttons','r');
  this.buf = Buffer.allocUnsafe(4);

  this.leds(0);
}

Newbook.prototype.calc_offset = function(ths) {
  if (ths == this.last_pressed) {
    this.offset += 4;
  } else {
    this.offset = 0;
  }
  this.last_pressed = ths;
  return this.offset;
}

Newbook.prototype.leds = function(val) {
  this.buf.writeUInt8(val, 0);
  fs.writeSync(this.cmd_fd,this.buf,0,1);
}

Newbook.prototype.i = function(val) {
  var offset = this.calc_offset(this.i);
  if (offset >= 8) {
    offset = this.offset = 0;
    this.ptr--;
  }
  if (offset == 0) 
    this.memory[this.ptr] = 0;
  this.memory[this.ptr] |= val << offset;
  this.inner_tick();
}

Newbook.prototype.l = function(val) {
  var offset = this.calc_offset(this.l);
  if (offset == 0) this.ptr = 0;
  this.ptr |= val << offset;
}

Newbook.prototype.o = function() {
  var offset = this.calc_offset(this.o);
  if (offset >= 8) {
    offset = this.offset = 0;
    this.ptr++;
  }
  if (this.memory[this.ptr] == undefined) 
    this.memory[this.ptr] = 0;
  var val = this.memory[this.ptr];
  val = (val >> (4 - offset)) & 0xF;
  this.leds(val);
}

Newbook.prototype.run = function() {
  var ths = this;
  var buttons = {16: ths.i, 
                 32: ths.l,
                 64: ths.o};
  var step = function() {
    if (fs.readSync(ths.btn_fd,ths.buf,0,1) > 0) {
      var val = ths.buf[0] & 0xF;
      var cmd = ths.buf[0] & 0x70;
      buttons[cmd].call(ths,val);
    }
    process.nextTick(step);
  }
  step();
}

Newbook.prototype.nam = function(str) {
  var ths = this;

  nam.parse(str,function(error,tokens,ign) {
    if (error != null) 
      return console.log(error);

    var time = 0;
    var dc = 0;
    var freq = 0;

    tokens.forEach(function(itm, idx, arr) {
      switch(itm.operator) {
        case '~':
          freq = itm.value;
          break;
        case '"':
          time = itm.value;
          break;
        case '%':
          dc = itm.value;
          break;
        case '_':
          ths.note(dc,1,time);
          break;
        case ']':
          if (itm.value != undefined)
            freq = itm.value;
          ths.note(dc,freq,time);
          break;
      }
    });

    ths.buf.writeUInt8(0x11, 0);
    fs.writeSync(ths.cmd_fd,ths.buf,0,1);

  });
}

Newbook.prototype.note = function(dc,freq,time) {
  if (dc == 12.5) dc = 0;
  if (dc == 25) dc = 1;
  if (dc == 50) dc = 2;
  freq = freq * 100;
  time = time*1000;
  if (time > 1000) time = 1000;
  var val = (dc << 30) | (freq << 10) | time;
  this.buf.writeInt32LE(val, 0)
  fs.writeSync(this.snd_fd,this.buf,0,4);
}

module.exports = new Newbook();
